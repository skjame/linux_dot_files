# A shortcut function that simplifies usage of xclip.
# - Accepts input from either stdin (pipe), or params.
# ------------------------------------------------
function cb() {
  local _scs_col="\e[0;32m"; local _wrn_col='\e[1;31m'; local _trn_col='\e[0;33m'
  # Check that xclip is installed.
  if ! type xclip > /dev/null 2>&1; then
    echo -e "$_wrn_col""You must have the 'xclip' program installed.\e[0m"
    return 2;
  # Check user is not root (root doesn't have access to user xorg server)
  elif [[ "$USER" == "root" ]]; then
    echo -e "$_wrn_col""Must be regular user (not root) to copy a file to the clipboard.\e[0m"
  else
    # If no tty, data should be available on stdin
    if ! [[ "$( tty )" == /dev/* ]]; then
      input="$(< /dev/stdin)"
    # Else, fetch input from params
    else
      input="$*"
    fi
    if [ -z "$input" ]; then  # If no input, print usage message.
      echo "Copies a string to the clipboard."
      echo "Usage: cb <string>"
      echo "       echo <string> | cb"
      echo "       cbo - get to stdout"
      echo "Alias cbwd (pwd)"
      echo "Alias cbf to buffer from file"
      echo "Alias cbmd5 copy md5sum"

    else
      # Copy input to clipboard
      echo -n "$input" | xclip -selection c
      # Truncate text for status
      if [ ${#input} -gt 80 ]; then input="$(echo $input | cut -c1-80)$_trn_col...\e[0m"; fi
      # Print status.
      echo -e "$_scs_col""Copied to clipboard:\e[0m $input"
    fi
  fi
}

function cbo(){
    echo `xclip -o -selection clipboard`
}

# Aliases / functions leveraging the cb() function
# ------------------------------------------------
# Copy contents of a file
function cbf() { cat "$1" | cb; }

# Copy contents which I copy from mcedit to mcbuf
function cbmc() {
    local mcedit_buffer="$HOME/.local/share/mc/mcedit/mcedit.clip"
    if [ ! -f "$mcedit_buffer" ]; then
        echo "Has no such file $mcedit_buffer";
        return 1;
    fi
    cat "$mcedit_buffer" | cb;
}

function tmux_3p(){
    $HOME/tmux.start.sh
}

# Copy SSH public key
# alias cbssh="cbf ~/.ssh/id_rsa.pub"
# Copy current working directory
alias cbwd="pwd | cb"
# Copy most recent command in bash history
alias cblc="cat $HISTFILE | tail -n 1 | cb"
alias cbmd5="hashdeep "$1" | cb"

function md5_check_test(){
    local file="$1"
    local checksum="$2"
    local calc="$(hashdeep "$file")"
    if [ "$checksum" == "$calc" ]; then
        echo "All ok! ;)"
        exit 0
    fi

    echo "Compare FAILED! :'("
    exit 1
}

# tags for mcedit.

function make-tags(){
    if ! type ctags > /dev/null 2>&1; then
    echo -e "$_wrn_col""You must have the 'ctags' program installed.\e[0m"
    echo "sudo apt install exuberant-ctags";
    return 2;
    fi

    if [ $# -eq 0 ]; then
    echo "Usage: make_tags <directory with sources> <language(optional)>"
    echo "TAGS file will be maked recursively"
    langlist=$(ctags --list-languages);
    echo "Supported languages list:$langlist";
    return 1
    fi

    lang="C";
    if [ $# -eq 2 ]; then
    lang=$2;
    fi
    path=$(realpath $1);
    if [ "$path" = "" ]; then
        path=$(dirname $0);
    fi
    echo "Choosed language: ${lang}";
    echo "Choosed path: ${path}";
    ctags -e --language-force=${lang} -R $1
    return $?
}

function targz_unpack(){
    local dest="$2"
    local src="$1"
    tar xf "$src" -C "$dest";
    cd "$dest"
}

function pycharm(){
    /opt/pycharm/bin/pycharm.sh
}

function replace_recursively() {
    where="$1"
    prev="$2"
    new="$3"

    if [ "$where" == "" ] || [ "$prev" == "" ] || [ "$new" == "" ]; then
        echo "$0 <where> <old_value> <new_value>"
        return 1
    fi

    uname -a | grep "MacBook" &> /dev/null
    isItMacos=$?

    if [ "$isItMacos" == "0" ]; then
        sed_app=gsed
    else
        sed_app=sed
    fi

    grep -rl "$old_value" $where | xargs $sed_app -i "s/$prev/$new/g"
}

function carwall_build_project() {
	# check if we in root of carwall
	ls -la | grep ".git" &> /dev/null
	if [ $? -ne 0 ]; then
		echo "You should be in carwall git repo dir. No .git folder here."
		return 1
	fi

	git remote show origin | grep git@bitbucket.org:karambasecurity/carwall.git &> /dev/null
	if [ $? -ne 0 ]; then
		echo "You should be in carwall git repo dir. Wrong origin."
		return 2
	fi

	if [ "$1" == "" ]; then
		echo "cmd require 1 paramatere: project name"
		return 3
	fi

	~/parasoft/scripts/analyze_karamba.sh -p "$1" -c Release -x c $(pwd)
	return $?
}

kbuild() {
	# 1 project
	# 2 config
	docker run -it -t --rm -v "$(pwd)":/home/user/carwall --tmpfs /tmp 192.168.1.21:5000/carwall-build bash -c "codelite-make -w ./whitebin.workspace -p $1 -c $2 && make clean && make -j16"
	sed -i "s|/home/user/carwall|$(pwd)|g" ./compile_commands.json
}

export WORKSPACEDIRMY="/home/devsrv/karamba/git/projects/xguard/carwall/src/whitebin/"
export BUILDDIRMY="/home/devsrv/karamba/build_env"
export INSIDEQEMU="/home/devsrv/karamba/build_env/qemu-buildroot/output/build/karamba_agent/AgentLinux"
