set nocompatible

syntax on

" Vundle begins here; turn off filetype temporarily
" set the runtime path to include Vundle and initialize
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" plugins
Plugin 'scrooloose/nerdtree'
" Plugin 'tpope/vim-surround'
"Plugin 'ctrlpvim/ctrlp.vim'
"Plugin 'christoomey/vim-tmux-navigator'
Plugin 'itchyny/vim-gitbranch'
Plugin 'itchyny/lightline.vim'
Plugin 'scrooloose/nerdcommenter'
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-markdown'
Plugin 'flazz/vim-colorschemes'
Plugin 'bronson/vim-trailing-whitespace'
Plugin 'DoxygenToolkit.vim'
Plugin 'ycm-core/YouCompleteMe'
Plugin 'romainl/vim-qf'
" let Vundle manage Vundle, required
Plugin 'skywind3000/asyncrun.vim'
Plugin 'zivyangll/git-blame.vim'

" vim markdown tables aligh
Plugin 'junegunn/vim-easy-align'


Plugin 'VundleVim/Vundle.vim'

call vundle#end()
filetype plugin indent on

augroup vimrc
    autocmd QuickFixCmdPost * botright copen 8
augroup END

set term=xterm-256color
"screen-256color
colorscheme molokai

let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'
let g:UltiSnipsExpandTrigger="<c-space>"

set autoindent
" set bg=dark
set backspace=indent,eol,start
set ignorecase
set incsearch
set laststatus=2
set linebreak
set nobackup
set noerrorbells
set nolist
set noswapfile
set novb
set nowrap
set number
" set relativenumber
" set ruler
set scrolloff=8
set showmatch
set shiftwidth=4
set shortmess=I
set tabstop=4
set softtabstop=4
set showcmd
set noshowmode
set sidescroll=1
set sidescrolloff=7
set smartcase
set softtabstop=4
set undolevels=1000
set nrformats-=octal
" set vb

let &path.="Source/Include,src/include,/usr/include/AL,"

augroup project
    autocmd!
    autocmd BufRead,BufNewFile *.h,*.c set filetype=c.doxygen
augroup END

highlight ColorColumn ctermbg=black
set colorcolumn=180

let g:syntastic_python_flake8_args = "--ignore=E501 --max-complexity 10"

"CtrlP
let g:ctrlp_map = '<c-p>'

"Airline
let g:airline_theme='tomorrow'
let g:airline_powerline_fonts = 1

"NERDTree
map <C-n> :NERDTreeToggle<CR>

let test#python#runner = 'pytest'

"Gvim mods
set encoding=utf-8
set hidden
set history=100
set mouse=a

"set guioptions-=m
"set guioptions-=T
"set guioptions-=r
"set guioptions-=Lo

"nerd-commenter settings
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Align line-wise comment delimiters flush left instead of following code
" indentation
let g:NERDDefaultAlign = 'left'

" Allow commenting and inverting empty lines (useful when commenting a
" region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l


nnoremap <F2> :YcmCompleter GoToImprecise<CR>
nnoremap <F3> :YcmCompleter GoToDeclaration<CR>
nnoremap <F4> :YcmCompleter GoToInclude<CR>
nnoremap <F5> :YcmForceCompileAndDiagnostics<CR>

" nmap <silent> <leader>t : TestNearest<CR>
" nmap <silent> <leader>T : TestFile<CR>
" nmap <silent> <leader>a : TestSuite<CR>
" nmap <silent> <leader>l : TestLast<CR>
" nmap <silent> <leader>v : TestVisit<CR>

" map gn :bn<cr>
" map gp :bp<cr>
" map gd :bd<cr>

" Remove all trailing whitespace by pressing Ctrl+t
noremap <C-t> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>

"if has("gui_running")
"    if has("gui_gtk2")
"        set guifont=Inconsolata\ for\ Powerline\ Medium\ 16
"        colorscheme iceberg
"    endif
"endif
let NERDTreeShowHidden=1

let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name'
      \ },
      \ }


nnoremap <Leader>s :<C-u>call gitblame#echo()<CR>

let g:DoxygenToolkit_briefTag_pre="@brief	"
let g:DoxygenToolkit_paramTag_pre="@param	"
let g:DoxygenToolkit_returnTag="@return	"

" vim-easy-align - markdown table align
" Align GitHub-flavored Markdown tables Bslash au FileType markdown
nnoremap <Leader>a :EasyAlign*<Bar><Enter>

nnoremap <Space>r :registers<CR>

